# Ansible role - CSIRT-MU OIDC provider

This role sets package managers to use CSIRT-MU OIDC provider

## Requirements

* This role requires root access, so you either need to specify `become` directive as a global or while invoking the role.

    ```yml
    become: yes
    ```

## Role paramaters

Mandatory parameters.

* `oidc_host` - The IP address or fully qualified domain name of the target machine.
* `oidc_git_ssh_key` - The SSH private key that is used to access source code of [OIDC-provider](https://gitlab.ics.muni.cz/CSIRT-MU/oidc-auth/csirtmu-oidc-overlay) from Git repository.
* `oidc_ssl_certificate` - The SSL certificate that is used for HTTPS communication.
* `oidc_ssl_certificate_key` - The key to SSL certificate `oidc_ssl_certificate`.

Optional parameters.

* `oidc_port` - The port of OIDC provider service (default: 8443).
* `oidc_git_repository_version` -  The version of the OIDC provider Git repository (default: master).

## Example

Example of the simplest OIDC provider configuration.

```yml
roles:
    - role: oidc
      oidc_host: example.com
      oidc_git_ssh_key: ~/.ssh/git-private-key
      oidc_ssl_certificate: ~/.certificates/csirt-mu-oidc-provider.crt
      oidc_ssl_certificate_key: ~/.certificates/csirt-mu-oidc-provider.key
```
